window.addEventListener("DOMContentLoaded", function () 
{
    const PRICE_1 = 25;
    const PRICE_2 = 40;
    const PRICE_3 = 20;
    const EXTRA_PRICE = 0;
    const RADIO_1 = 0;
    const RADIO_2 = 5;
    const RADIO_3 = 7;
    const CHECKBOX = 10;
    
    let price = PRICE_1;
    let extraPrice = EXTRA_PRICE;
    let result;
    
    let calc = document.getElementById("calc");
    let num = document.getElementById("number");
    let resultSpan = document.getElementById("result-span");
    let select = document.getElementById("select");
    let radiosDiv = document.getElementById("radio-div");
    let radios = document.querySelectorAll("#radio-div input[type=radio]");
    let checkboxDiv = document.getElementById("check-div");
    let checkbox = document.getElementById("checkbox");
    
    select.addEventListener("change", function (event) 
	{
        let option = event.target;
        if (option.value === "1") 
		{
            radiosDiv.classList.add("d-none");
            checkboxDiv.classList.add("d-none");
            extraPrice = EXTRA_PRICE;
            price = PRICE_1;
        }
        if (option.value === "2") 
		{
            radiosDiv.classList.remove("d-none");
            checkboxDiv.classList.add("d-none");
            extraPrice = RADIO_1;
            price = PRICE_2;
            document.getElementById("radio1").checked = true;
        }
        if (option.value === "3") 
		{
            checkboxDiv.classList.remove("d-none");
            radiosDiv.classList.add("d-none");
            extraPrice = EXTRA_PRICE;
            price = PRICE_3;
            checkbox.checked = false;
        }
    });
    radios.forEach(function (currentRadio) {
        currentRadio.addEventListener("change", function (event) {
            let radio = event.target;
            if (radio.value === "r1") 
			{
                extraPrice = RADIO_1;
            }
            if (radio.value === "r2") 
			{
                extraPrice = RADIO_2;
            }
            if (radio.value === "r3") 
			{
                extraPrice = RADIO_3;
            }
        });
    });
    checkbox.addEventListener("change", function () {
        if (checkbox.checked) 
		{
            extraPrice = CHECKBOX;
        } else 
		{
            extraPrice = EXTRA_PRICE;
        }
    });

    calc.addEventListener("change", function () 
	{
        if (num.value < 1) 
		{
            num.value = 1;
        }
        result = (price + extraPrice) * num.value;
        resultSpan.innerHTML = result;
    });
});